import { DefaultCrudRepository } from '@loopback/repository';
import { Billing } from '../models';
import { db } from '../datasources/db.datasource';

export class BillingRepository extends DefaultCrudRepository<Billing, typeof Billing.prototype.id> {
  constructor() {
    super(Billing, db);
  }
}
