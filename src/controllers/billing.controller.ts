import { inject } from '@loopback/core';
import { api } from '@loopback/rest';
import { billingApi } from '../apidefs';
import { Billing } from '../models';
import { BillingRepository } from '../repositories';

@api(billingApi)
export class BillingController {
  constructor(
    //@inject('repositories.BillingRepository') private repository: BillingRepository
  ) { }

  async hi() {
    return 'Hello world';
  }
}
