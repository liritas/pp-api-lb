import { inject } from '@loopback/core';
import { get, ServerRequest } from '@loopback/rest';
//import { AdyenApiJs } from 'adyen-api-js';
const https = require('http');
var request = require('request');
var querystring = require('querystring');

export class AdyenController {
  constructor( @inject('rest.http.request') private req: ServerRequest) { }

  @get('/adyen')
  adyen() {
    var options = {
      url: 'https://checkout-test.adyen.com/services/PaymentSetupAndVerification/v32/setup',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-API-Key': 'AQEjhmfuXNWTK0Qc+iTixjMVouCSSUCcjG5P0+BIgEYE2IugUHUQwV1bDb7kfNy1WIxIIkxgBw==-cJJf5HrTpRs73sZ1qqmKzBp6diZtcxIctkmnAm0fMLw=-IRB98nGV8P9QYtv3'
      },
      json: {
        returnUrl: "http://www.altoros.com",
        channel: "Web",
        countryCode: "NL",
        shopperLocale: "nl_NL",
        reference: "001",
        merchantAccount: "247MediaNL",
        amount: {
          currency: "EUR",
          value: 50
        }
      }
    };
    return request.post(options,
      function(error: any, response: any, body: any) {
        if (error) {
          console.log('error:', error); // Print the error if one occurred
          return error;
        }  else {
          //console.log('statusCode:', response && response.statusCode);
          let paymentMethods: any = body.paymentMethods;
          let ideal: any = paymentMethods.find(function (obj: any) { return obj.name === 'iDEAL'; });
          return response;
        }
      });
  }

  @get('/adyen-auth')
  adyen2() {
      let authData = new Buffer('ws@Company.247Media":pp163874').toString('base64');
      console.log(authData);
      var options = {
        url: 'https://pal-test.adyen.com/pal/servlet/Payment/v32/authorise',
        method: 'POST',
        headers: {
          'Authorization': 'Basic ' + authData,
          'Content-Type': 'application/json',
          //'X-API-Key': 'AQEjhmfuXNWTK0Qc+iTixjMVouCSSUCcjG5P0+BIgEYE2IugUHUQwV1bDb7kfNy1WIxIIkxgBw==-cJJf5HrTpRs73sZ1qqmKzBp6diZtcxIctkmnAm0fMLw=-IRB98nGV8P9QYtv3'
        },
        json: {
          //returnUrl: "http://www.altoros.com",
          //channel: "Web",
          //countryCode: "NL",
          //shopperLocale: "nl_NL",
          reference: "001",
          merchantAccount: "247MediaNL",
          amount: {
            currency: "EUR",
            value: 50
          }
        }
      };
      return request.post(options,
        function(error: any, response: any, body: any) {
          if (error) {
            console.log('error:', error);
            return error;
          }  else {
            //return response;
            return 'OK';
          }
        });
  }
}

// Client libruary location https://test.adyen.com/hpp/cse/js/8215139478112023.shtml
