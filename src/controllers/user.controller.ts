import { inject } from '@loopback/core';
import { api } from '@loopback/rest';
import {
  authenticate,
  AuthenticationMetadata,
  AuthenticationBindings,
  UserProfile,
} from '@loopback/authentication';
import { userApi } from '../apidefs';
import { UserRepository } from '../repositories';
import { User } from '../models';

export class UserController {
  constructor(
    @inject(AuthenticationBindings.CURRENT_USER) private user: UserProfile,
    @inject('repositories.UserRepository') private repository: UserRepository,
  ) { }

  @authenticate('BasicStrategy')
  async helloWorld(name?: string) {
    return `Hello world ${name}`;
  }

  @authenticate('BasicStrategy')
  async getUsers(name?: string): Promise<User[]> {
    let filter = name ? { where: { username: name } } : {};
    return await this.repository.find(filter);
  }

  @authenticate('BasicStrategy')
  async createUser(userInfo: Partial<User>) {
    return await this.repository.create(new User(userInfo));
  }
}
