import { Application, ApplicationConfig } from '@loopback/core';
import { AuthenticationComponent, AuthenticationBindings } from '@loopback/authentication';
import { validateApiSpec } from '@loopback/testlab';
import { RestComponent, RestServer, RestBindings } from '@loopback/rest';
import { AuthStrategyProvider } from './providers/auth.provider';
import {
  AdyenController,
  BillingController,
  UserController
} from './controllers';
import {
  BillingRepository,
  UserRepository
} from './repositories';
import { db } from './datasources/db.datasource';
import { Class, DataSourceConstructor, Repository, RepositoryMixin } from '@loopback/repository';
import { MySequence } from './sequence';
import { spec } from './apidefs/swagger';

export class PpApiLbApplication extends RepositoryMixin(Application) {
  constructor(options?: ApplicationConfig) {
    // Allow options to replace the defined components array, if desired.
    options = Object.assign(
      {},
      {
        components: [
          AuthenticationComponent,
          RestComponent
        ],
        rest: {
          sequence: MySequence
        },
        controllers: [
          AdyenController,
          BillingController,
          UserController
        ]
      },
      options,
    );
    super(options);
    this.server(RestServer);
    this.setupRepositories();
    this.setupControllers();
  }

  async start() {
    const server = await this.getServer(RestServer);
    server.sequence(MySequence);
    server.api(spec);
    const port = await server.get(RestBindings.PORT);
    console.log(`Server is running at http://127.0.0.1:${port}`);
    server.bind(AuthenticationBindings.STRATEGY).toProvider(AuthStrategyProvider);
    return await super.start();
  }

  setupRepositories() {
    // TODO(bajtos) Automate datasource and repo registration via @loopback/boot
    // See https://github.com/strongloop/loopback-next/issues/441
    const datasource =
      this.options && this.options.datasource
        ? new DataSourceConstructor(this.options.datasource)
        : db;
    this.bind('datasource').to(datasource);
    //this.repository(UserRepository);
  }
  setupControllers() {
    this.controller(AdyenController);
    this.controller(BillingController);
    this.controller(UserController);
  }
}
