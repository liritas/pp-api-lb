import { PpApiLbApplication } from './application';
import { ApplicationConfig } from '@loopback/core';

export { PpApiLbApplication };

export async function main(options?: ApplicationConfig) {
  const app = new PpApiLbApplication(options);

  try {
    await app.start();
  } catch (err) {
    console.error(`Unable to start application: ${err}`);
  }
  return app;
}
