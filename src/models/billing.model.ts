import { Entity, model, property } from '@loopback/repository';

@model()
export class Billing extends Entity {
  @property({
    description: 'The unique identifier for a transaction',
    type: 'number',
    id: true
  })
  id: number;

  @property({ type: 'string', required: true })
  provider: string;

  @property({ type: 'number', required: true })
  value: number;

  // Add the remaining properties yourself:
  // description, available, category, label, endData
}
