import {Entity, model, property} from '@loopback/repository';

@model()
export class User extends Entity {
  @property({
    description: 'The unique identifier for a user',
    type: 'number',
    id: true
  })
  id: number;

  @property({
    description: 'The username for a user.',
    required: true,
    default: 'Anonymous User',
    type: 'string',
  })
  username: string;

  @property({type: 'string', required: true})
  email: string;

  @property({type: 'string', required: true})
  password: string;

  // Add the remaining properties yourself:
  // description, available, category, label, endData
}
