import { inject, Provider, ValueOrPromise } from '@loopback/context';
import { AuthenticationBindings, AuthenticationMetadata } from '@loopback/authentication';
import { Strategy } from 'passport';
import { BasicStrategy } from 'passport-http';

import { User } from '../models';

export class AuthStrategyProvider implements Provider<Strategy | undefined> {
  constructor(
    @inject(AuthenticationBindings.METADATA)
    private metadata: AuthenticationMetadata,
    private users: User
  ) { }

  value(): ValueOrPromise<Strategy | undefined> {
    // The function was not decorated, so we shouldn't attempt authentication
    if (!this.metadata) {
      return undefined;
    }
    const name = this.metadata.strategy;
    if (name === 'BasicStrategy') {
      let a = this.users.find({
        where: {
          username: 'bla',
          password: 'yes'
        }
      });
      console.log(a);
      return new BasicStrategy(this.verify);
    } else {
      return Promise.reject(`The strategy ${name} is not available.`);
    }
  }

  verify(username: string, password: string, cb: Function) {
    // find user by name & password
    // call cb(null, false) when user not found
    // call cb(null, userProfile) when user is authenticated
    let userProfile = {
      username: username,
      password: password,
    };
    return cb(null, userProfile);
  }
}
