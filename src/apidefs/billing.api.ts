'use strict';

export const billingApi = {
  swagger: '2.0',
  paths: {
    '/hi': {
      get: {
        'x-operation-name': 'hi',
        'x-controller-name': 'BillingController',
        parameters: [
        ],
        responses: {
          '200': {
            description: 'Returns a hello world.',
            examples: {
              'text/plain': 'Hello world',
            },
          },
        },
      },
    },
    '/billing': {
      get: {
        'x-operation-name': 'getTransactions',
        'x-controller-name': 'BillingController',
        parameters: [
          {
            name: 'name',
            in: 'query',
            description: 'The name for the user instance.',
            required: false,
            type: 'string',
            'x-example': 'Ted',
          },
        ],
        responses: {
          '200': {
            description: 'An array of persisted billing instances.',
            schema: {
              type: 'array',
              items: {
                $ref: '#/definitions/billing',
              },
            },
            examples: {
              'application/json': '[{transaction:"Ted", id: 1}]',
            },
          },
        },
      },

      post: {
        'x-operation-name': 'createTransaction',
        'x-controller-name': 'BillingController',
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [
          {
            name: 'userInfo',
            in: 'body',
            description: 'The user model instance to create.',
            required: false,
            schema: {
              $ref: '#/definitions/billing',
            },
          },
        ],
        responses: {
          '200': {
            description: 'The created user instance.',
            schema: {
              $ref: '#/definitions/billing',
            },
            examples: {
              'application/json': '{"id": 1,"username": "Ted"}',
            },
          },
        },
      },
    },
  },
};
