import * as lbOpenApiSpec from '@loopback/openapi-spec';
import { OpenApiSpec } from '@loopback/openapi-spec';
import { billingApi, userApi } from './';

export let spec = lbOpenApiSpec.createEmptyApiSpec();
spec.info = {
  title: 'Huizenmarkt.nl API by Loopback 4',
  version: '2.0',
};
spec.swagger = '2.0';
spec.basePath = '/';

spec.definitions = {
  user: {
    type: 'object',
    properties: {
      id: {
        type: 'number',
        description: 'The ID for a user instance.',
      },
      username: {
        type: 'string',
        description: 'The username for a user instance.',
      },
    },
    required: ['username'],
    example: {
      username: 'Ted',
    },
  },
  billing: {
    type: 'object',
    properties: {
      id: {
        type: 'number',
        description: 'The ID for a billing instance.',
      },
      provider: {
        type: 'string',
        description: 'The provider name for a billing instance.',
      },
      currency: {
        type: 'string',
        description: 'The currency for a billing instance.',
      },
      amount: {
        type: 'number',
        description: 'The amount for a billing instance.',
      },
    },
    required: [
      'provider',
      'currency',
      'amount'
    ],
    example: {
      provider: 'ideal',
      currency: 'EUR',
      amount: '100',
    },
  },
};

spec = Object.assign({}, spec, billingApi);
spec = Object.assign({}, spec, userApi);
